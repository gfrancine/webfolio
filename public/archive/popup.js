// ARCHIVE POPUP
// Todo: a11y

const archivePopup = document.getElementById("archive-popup");
const archivePopupContent = archivePopup.querySelector(".content");
const archiveCloseButton = archivePopup.querySelector(".close");
const archiveHashes = [];
const navbar = document.querySelector(".navbar")

document.querySelectorAll(".archive-gallery .work").forEach((item) => {
  if (item.href) return; // don't show a popup if it's already a link to something
  item.href = "#archive-" + item.id; // a prefix, so we don't scroll to the thumbnail
  archiveHashes.push("#archive-" + item.id);
});

function closeArchivePopup() {
  archivePopup.classList.remove("visible");
  document.body.classList.remove("noscroll")
  history.pushState({}, "", "#"); // change hash without scrolling
  navbar.classList.remove("hide")
  setTimeout(() => archivePopup.remove(), 0) // CSS transition
}

function isWithin(point, rect) {
  // point: {x y}, rect: {left right top bottom}
  return (
    point.x >= rect.left &&
    point.x <= rect.right &&
    point.y >= rect.top &&
    point.y <= rect.bottom
  );
}

archivePopup.addEventListener("click", (e) => {
  // close popup when overlay is clicked
  const point = { x: e.clientX, y: e.clientY };
  if (!isWithin(point, archivePopupContent.getBoundingClientRect()))
    closeArchivePopup();
});

archiveCloseButton.addEventListener("click", (e) => {
  e.stopPropagation(); // stop it from entering the popup overlay event listener
  closeArchivePopup();
});

function updateArchivePopup() {
  if (archiveHashes.includes(window.location.hash)) {
    navbar.classList.add("hide") // css animation in main messes with the z index
    archivePopup.parentElement = document.body.querySelector("main");
    archivePopupContent.innerHTML = "";

    const item = document.getElementById(
      window.location.hash.slice("#archive-".length)
    );

    archivePopupContent.append(
      ...Array.from(item.children).map((el) => {
        if (el.classList.contains("title")) {
          const heading = document.createElement("h2");
          heading.innerHTML = el.innerHTML;
          heading.className = "title";
          return heading;
        } else {
          return el.cloneNode(true);
        }
      })
    );

    document.body.classList.add("noscroll")
    document.body.querySelector("main").append(archivePopup)
    archivePopup.classList.add("visible");
  } else {
    closeArchivePopup();
  }
}

closeArchivePopup(); // remove at start
window.addEventListener("hashchange", updateArchivePopup);
updateArchivePopup();
